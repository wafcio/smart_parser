# frozen_string_literal: true

require "spec_helper"

RSpec.describe SmartParser::PrepareStats do
  describe ".new" do
    it "initialize with collection" do
      expect { SmartParser::PrepareStats.new([]) }.to_not raise_error
    end
  end

  describe "#call" do
    let(:collection) do
      [
        ["/contact", "184.123.665.067"],
        ["/help_page/1", "126.318.035.038"],
        ["/help_page/1", "126.318.035.038"]
      ]
    end

    let(:subject) { SmartParser::PrepareStats.new(collection) }

    let(:output) do
      {
        visits: [
          ["/help_page/1", 2],
          ["/contact", 1]
        ],
        unique_visits: [
          ["/help_page/1", 1],
          ["/contact", 1]
        ]
      }
    end

    it "returns valid output" do
      expect(subject.call).to eq(output)
    end
  end
end
