# frozen_string_literal: true

require "spec_helper"

RSpec.describe SmartParser::ParseContent do
  describe ".new" do
    it "initialize with content" do
      expect { SmartParser::ParseContent.new("foo") }.to_not raise_error
    end
  end

  describe "#call" do
    let(:content) do
      [
        "/help_page/1 126.318.035.038",
        "/contact 184.123.665.067"
      ].join("\n")
    end

    let(:subject) { SmartParser::ParseContent.new(content) }

    let(:output) do
      [
        ["/help_page/1", "126.318.035.038"],
        ["/contact", "184.123.665.067"]
      ]
    end

    it "returns splitten array" do
      result = subject.call

      expect(result).to eq(output)
    end
  end
end
