# frozen_string_literal: true

require "spec_helper"

RSpec.describe SmartParser::LoadFile do
  describe ".new" do
    it "initialize with path to file" do
      expect { SmartParser::LoadFile.new("foo") }.to_not raise_error
    end
  end

  describe "#call" do
    let(:subject) { SmartParser::LoadFile.new(path) }

    context "when file doesn't exist" do
      let(:path) { "foo" }

      it "raises error" do
        expect { subject.call }.to raise_error(StandardError).with_message("File doesn't exist")
      end
    end

    context "when file exists" do
      let(:path) { "spec/assets/logs.log" }

      it "doesn't raise error" do
        expect { subject.call }.not_to raise_error
      end

      it "returns context of file" do
        result = subject.call

        expect(result).to eq("/help_page/1 126.318.035.038\n")
      end
    end
  end
end
