# frozen_string_literal: true

class SmartParser::ParseContent
  def initialize(content)
    @content = content
  end

  def call
    content
      .split("\n")
      .map(&:split)
  end

  private

  attr_reader :content
end
