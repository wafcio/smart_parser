# frozen_string_literal: true

class SmartParser::PrepareStats
  def initialize(collection)
    @collection = collection
  end

  def call
    {
      visits: prepare_visits(collection),
      unique_visits: prepare_visits(collection.uniq)
    }
  end

  private

  attr_reader :collection

  def prepare_visits(scope)
    visits = {}
    scope.each do |element|
      visits[element.first] ||= 0
      visits[element.first] += 1
    end

    visits.to_a.sort_by(&:last).reverse
  end
end
