# frozen_string_literal: true

class SmartParser::LoadFile
  def initialize(path)
    @path = path
  end

  def call
    raise StandardError, "File doesn't exist" unless File.exist?(path)

    File.read(path)
  end

  private

  attr_reader :path
end
