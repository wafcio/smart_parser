# frozen_string_literal: true

module SmartParser
  require_relative "smart_parser/load_file"
  require_relative "smart_parser/parse_content"
  require_relative "smart_parser/prepare_stats"

  def self.call(path)
    content = SmartParser::LoadFile.new(path).call
    collection = SmartParser::ParseContent.new(content).call
    SmartParser::PrepareStats.new(collection).call
  end
end
