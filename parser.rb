#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative "lib/smart_parser"

data = SmartParser.call(ARGV[0])

data[:visits].each do |element|
  puts "#{element.first} #{element.last} visits"
end
puts

data[:unique_visits].each do |element|
  puts "#{element.first} #{element.last} unique views"
end
